import sys
import os
import json
from occam import Occam

# Load OCCAM information
object = Occam.load()

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()
object_path = "/occam/%s-%s" % (object.id(), object.revision())
# Path to your executable
executable_path="plot.py"


sys.path.append(object_path)

#########################################################
#                                                       #
#           OCCAM gives us the configuration            #
#                                                       #
#########################################################
#dataset_configurations_path = object.configuration_file("Dataset Options")
figure_configurations_path = object.configuration_file("Figure Options")
inputs_configurations_path = object.configuration_file("Inputs Options")
with open(figure_configurations_path) as json_file:
    figure_configurations=json.load(json_file)

with open(inputs_configurations_path) as json_file:
    inputs_configurations=json.load(json_file)

file_type=inputs_configurations.get("type")


#########################################################
#                                                       #
#          The only input is the file                   #
#                                                       #
#########################################################
# Set default input file
if(file_type=='JSON'):
    input_type = 'application/json'
    default_input_file_path="example_input.json"
elif(file_type=='CSV'):
    input_type = 'application/csv'
    default_input_file_path="example_input.csv"

default_input_file_path=os.path.join(object_path,default_input_file_path)
input_files_path=[]
inputs = object.inputs(input_type)

if len(inputs) > 0:
    for i in range(0, len(inputs)):
        if(inputs[i]._object.get("created")):
            volume_dir_path = inputs[i].volume()
            if os.path.exists(os.path.join(volume_dir_path, 'object.json')):
                with open(os.path.join(volume_dir_path, 'object.json')) as json_file:
                    gen_object=json.load(json_file)
                    files = gen_object.get("files")
                    file = gen_object.get("file","")
                    if len(files) > 0:
                        input_file_path=os.path.join(volume_dir_path,files[0])
                        input_files_path.extend([input_file_path])
                    elif file !="":
                        input_file_path=os.path.join(volume_dir_path,file)
                        input_files_path.extend([input_file_path])
        else:
            files = inputs[i].files()
            file = inputs[i]._object.get("file","")
            if len(files) > 0:
                input_file_path = files[0]
                input_files_path.extend([input_file_path])
            elif file!="":
                input_file_path = os.path.join(inputs[i].volume(),file)
                input_files_path.extend([input_file_path])
if len(input_files_path)==0:
    input_files_path.extend([default_input_file_path])
#########################################################
#                                                       #
#           The output goes in this directory           #
#                                                       #
#########################################################
# Output file dir and path
output_folder="new_output"
output_dir_path = os.path.join(object.path(), output_folder)
## Check if object is connected to another block
connection_to_block = object.outputs("application/json")
if len(connection_to_block) > 0:
    output_block = connection_to_block[0]
    output_dir_path = output_block.volume()
## Create dir if needed
if not os.path.exists(output_dir_path):
    os.mkdir(output_dir_path);

#########################################################
#                                                       #
#                   Build run command                   #
#                                                       #
#########################################################
executable=os.path.join(object_path,executable_path)
args=[
    "python",
    executable,
    inputs_configurations_path,
    figure_configurations_path,
    output_dir_path,
    str(len(input_files_path)),
    " ".join(input_files_path)
]
command = ' '.join(args)
Occam.report(command)

# import plot
