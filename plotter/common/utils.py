from copy import deepcopy
import re

def safe_math_eval(string):
    parentheses = re.compile("\(.*\)")
    found = parentheses.search(string.strip())
    while found:
        # print("---->%s"%string)
        hit=found.group(0)
        string=parentheses.sub(safe_math_eval(hit[1:-1]), string, count=1)
        # print("<----%s"%string)
        found = parentheses.search(string.strip())
    prog_times_div = re.compile("[+-]?\s*\d+\s*[\/\*]\s*[+-]?\s*\d+")
    found = prog_times_div.search(string.strip())
    while found:
        # print("---->%s"%string)
        hit=found.group(0)
        result = int(eval(hit))
        string=prog_times_div.sub(["-", "+"][result > 0]+str(result), string, count=1)
        # print("<----%s"%string)
        found = prog_times_div.search(string.strip())
    prog_add_sub = re.compile("[+-]?\s*\d+\s*[+-]+\s*\d+")
    found = prog_add_sub.search(string.strip())
    while found:
        # print("---->%s"%string)
        hit=found.group(0)
        result = int(eval(hit))
        string=prog_add_sub.sub(["-", "+"][result > 0]+str(result), string, count=1)
        # print("<----%s"%string)
        found = prog_add_sub.search(string.strip())
    return string


def parse_range(input_string, number_of_elements=-1):
    # split by ","
    split_input_string = str(input_string).split(",")
    list_of_numbers=[]
    for element in split_input_string:
        if(element == ":"):
            range_start=0
            range_step=1
            range_end=number_of_elements-1
        else:
            # Split by ":"
            split_element = element.split(":")
            # Check if ends in "end"
            if(split_element[-1].find("end") >= 0):
                split_element[-1]=split_element[-1].lower().replace("end", str(number_of_elements-1))
                split_element[-1]=safe_math_eval(split_element[-1])
            # Get "start" and "end"
            range_start=int(split_element[0])
            range_step=1
            range_end=int(split_element[-1])
            # Check if there is a step
            if(len(split_element)==3):
                range_step=int(split_element[1])
        for i in range(range_start, range_end+1, range_step):
            list_of_numbers.append(i)
    list_of_numbers = sorted(list(set(list_of_numbers)))
    return list_of_numbers
