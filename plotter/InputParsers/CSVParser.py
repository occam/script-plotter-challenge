# from .AbstractParser import AbstractParser
import os
import csv


class CSVParser:#(AbstractParser):

    def __init__(self):
        return;

    def parse(self,input_file, options={"delimiter":",","quotechar":'"'}):
        processed_csv = None
        parsed_csv = []
        with open(input_file) as cvs_input:
            processed_csv = csv.reader(
                cvs_input,
                delimiter=str(options.get("delimiter")),
                quotechar=str(options.get("quotechar"))
            )
            for row in processed_csv:
                parsed_row=[]
                for entry in row:
                    parsed_row.append(entry)
                parsed_csv.append(parsed_row)
        return parsed_csv

#pdb.set_trace()
