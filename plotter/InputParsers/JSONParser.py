# from .AbstractParser import AbstractParser
import os
import json
import re
import copy
from plotter.common.utils import parse_range

class JSONParser:#(AbstractParser):


    def __init__(self):
        self.parsed_json = None
        return;

    @staticmethod
    def flatten_arrays(array):
        f_array=[]
        if(len(array)>0):
            if(isinstance(array[0], list)):
                for element in array:
                    ret = JSONParser.flatten_arrays(element)
                    f_array.extend(ret)
            else:
                f_array.append(array)
        return f_array
    @staticmethod
    def flatten(array):
        f_array=[]
        if(len(array)>0):
            if(isinstance(array[0], list)):
                for element in array:
                    ret = JSONParser.flatten(element)
                    f_array.extend(ret)
            else:
                f_array.extend(array)
        return f_array

    @staticmethod
    def convert_ranges(string, parsed_json, previous_path=''):
        # example input => experiment(:).variable_of_interest(1:2:end)
        output = copy.deepcopy(parsed_json);
        prog = re.compile("\([\d:+-\/\*\s(end)]+\)")
        split_path = string.partition(".")
        ## portion '.' remaining....
        portion = split_path[0]
        remaining=split_path[2:]
        # print(remaining)
        remaining=''.join(str(i) for i in remaining)
        # print("remaining")
        # print(remaining)
        # print("portion")
        # print(portion)
        # Get all index references
        #   e.g. '(:)', '(1:2:end)'
        hit=prog.search(portion)
        if hit:
            hit=hit.group(0)
            # print("element=%s"%output)
            hit_portion = portion.partition(hit)[0]
            subelements = output.get(hit_portion)
            replacer=parse_range(hit[1:-1],len(subelements))
            # print("replacer: %s"%replacer)
            temp_out=[]
            for replacer_idx in replacer:
                subelement=subelements[replacer_idx]
                # print("subelement=%s"%subelement)
                # print("path=%s"%(remaining))
                out = JSONParser.convert_ranges(remaining,subelement, "%s%s(%d)."%(previous_path,hit_portion, replacer_idx))
                temp_out.append(out)
            if len(temp_out)==1:
                temp_out=temp_out[0]
            output = temp_out
            # print("temp: %s"%(temp_out))
        elif remaining!="":
            output = JSONParser.convert_ranges(remaining,output.get(portion), "%s%s."%(previous_path,portion))
        elif portion!="":
            print ("portion: %s"%str(portion))
            output = output.get(portion)
        # print ("output: %s"%str(output))
        # print ("path: %s%s"%(previous_path,portion))
        return output



    def parse(self,input_file, options={"delimiter":",","quotechar":'"'}):


        self.parsed_json = json.load(open(input_file,'r'))
        print(self.parsed_json)
        return self.parsed_json


    def get(self, x_path=None, y_path=None, legend_path=None):
        if y_path == None and x_path != None:
            y_path = x_path
            x_path = None


        n_lines = 1

        if y_path:
            y_data = JSONParser.convert_ranges(y_path, self.parsed_json)
            if not isinstance(y_data, list):
                y_data=[y_data]
            # y_data=JSONParser.flatten_arrays(y_data)
        if x_path:
            x_data = JSONParser.convert_ranges(x_path, self.parsed_json)
            if not isinstance(x_data, list):
                x_data=[x_data]
            # x_data=JSONParser.flatten_arrays(x_data)
        else:
            x_data = []
            # Chech if y is a list of lists (aka a list of lines)
            #   e.g. [[1,2,3], [1,4,9,16]]
            if isinstance(y_data[0], list):
                for line in y_data:
                    x_line=[]
                    for i in range(0,len(line)):
                        x_line.append(i)
                    x_data.append(x_line)
            # Else y is a single line
            #   e.g. [1,2,3]
            else:
                for i in range(0,len(line)):
                    x_data.append(i)

        legend_data=[]
        if legend_path:
            legend_data = JSONParser.convert_ranges(legend_path, self.parsed_json)
            if not isinstance(legend_data, list):
                legend_data=[legend_data]
            # legend_data=JSONParser.flatten(legend_data)


        if(len(y_data)<=0):
            print("Error: no data provided")
            return None, None, None
        else:
            # Chech if y is a list of lists (aka a list of lines)
            #   e.g. [[1,2,3], [1,4,9,16]]
            if isinstance(y_data[0], list):
                # If there is only one x_data, then replicate
                if len(x_data)==1:
                    x_temp=copy.deepcopy(x_data)
                    for i in range(0, len(y_data)-1):
                        x_data.append(x_temp)
                # Make sure the sizes match
                if(len(x_data)!=len(y_data)):
                    print("Error: The number of lines in x (%d) must match the number of lines in y (%d)"%(len(x_data), len(y_data)))
                    return None, None, None
                for i in range(0, len(y_data)):
                    if len(y_data[i])!=len(x_data[i]):
                        print("Error: The number of points in x (%d) must match the number of points in y (%d)"%(len(x_data[i]), len(y_data[i])))
                        return None, None, None
            else:
                if(len(x_data)!=len(y_data)):
                    print("Error: The number of lines in x (%d) must match the number of lines in y (%d)"%(len(x_data), len(y_data)))
                    return
                x_data=[x_data]
                y_data=[y_data]

            if isinstance(y_data[0], list):
                if len(legend_data)<len(y_data):
                    legend_temp=[]
                    for i in range(0, len(y_data)):
                        try:
                            legend_data[i]=legend_data[i]
                        except:
                            legend_data.append("")
            else:
                try:
                    legend_data=[legend_data]
                except:
                    legend_data=[""]

        return x_data, y_data, legend_data

#pdb.set_trace()
